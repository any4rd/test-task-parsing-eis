from celery import group

from celery_tasks import CollectLinksTask, ParseXmlTask

page_urls = [
    'https://zakupki.gov.ru/epz/order/extendedsearch/results.html?fz44=on&pageNumber=1',
    'https://zakupki.gov.ru/epz/order/extendedsearch/results.html?fz44=on&pageNumber=2'
]

collect_links_task = group(CollectLinksTask().s(url) for url in page_urls)

for link in collect_links_task.apply_async():
    parse_xml_task = group(
        ParseXmlTask().s(tender_number, tender_url)
        for tender_number, tender_url in link.get()
    )

    for xml_result in parse_xml_task.apply_async():
        print(xml_result.get())
