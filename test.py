import re

import requests
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup

session_chrome = requests.Session()
session_chrome.headers.update({'User-Agent': 'Chrome/58.0.3029.110'})


def links_tasks(page_url):
    response = session_chrome.get(page_url)
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')

        pattern = r'/epz/order/notice/printForm/view.html\?regNumber=(\d+)'
        tenders_info = []
        for link in soup.find_all('a', href=re.compile(pattern)):
            match = re.search(pattern, link['href'])
            if match:
                tender_number = match.group(1)
                tender_url = 'https://zakupki.gov.ru' + link['href']
                tenders_info.append((tender_number, tender_url))

        return tenders_info
    else:
        raise Exception("Ошибка при получении страницы.")


def xml_tasks(tender_number, tender_url):
    xml_url = f'https://zakupki.gov.ru/epz/order/notice/printForm/viewXml.html?regNumber={tender_number}'
    response = session_chrome.get(xml_url)
    if response.status_code == 200:
        root = ET.fromstring(response.content)
        namespaces = {
            'ns7': 'http://zakupki.gov.ru/oos/printform/1',
            'default': 'http://zakupki.gov.ru/oos/EPtypes/1'
        }
        publish_dt_in_eis = root.find(".//default:publishDTInEIS", namespaces)

        if publish_dt_in_eis is not None and publish_dt_in_eis.text:
            publish_dt_value = publish_dt_in_eis.text
        else:
            publish_dt_value = None

        return f"Ссылка на печатную форму: {tender_url} - Дата публикации: {publish_dt_value}"
    else:
        raise Exception(f'Ошибка при получении XML документа для тендера {tender_number}.')


if __name__ == '__main__':
    page_url = [
        'https://zakupki.gov.ru/epz/order/extendedsearch/results.html?fz44=on&pageNumber=1',
        'https://zakupki.gov.ru/epz/order/extendedsearch/results.html?fz44=on&pageNumber=2'
    ]

    for i in page_url:
        result = links_tasks(i)
        for tender_number, tender_url in result:
            print(xml_tasks(tender_number, tender_url))

